-- GBA Shaman King - Master of Spirits, TASing script
-- Ram watch, Yoh and enemy information
-- Warning: shit quality; first time lua
-- Written by xy2_, 2017

local color= {
	opaque= {
		[0]=0xFF00FF00, -- Green
		0xFFFFFF00, -- Yellow
		0xFFFF0000, -- Red
		0xFFBA8E7D, -- Brown
		0xFF0000FF, -- Blue
		0xFF665046  -- Dark brown
	};
	trans= {
		[0]=0x7700FF00, -- Green
		0x77FFFF00, -- Yellow
		0x77FF0000, -- Red
		0x77BA8E7D, -- Brown
		0x770000FF, -- Blue
		0x44665046  -- Dark brown
	}
}

client.SetGameExtraPadding(0,0,40,0)

-- General RAM watch, and value display on screen
--#############################################################################
local function DisplayHud(x,y)
--#############################################################################

	-- Furyoku
	local furyoku     = memory.read_u16_le(0x22BE, "IWRAM")
	gui.pixelText (x+0,y+0, string.format("%4d", furyoku), 0xFFFFFFFF, color.trans[4])

	-- Furyoku refill
	local frefill     = memory.read_u16_le(0x22CA, "IWRAM")
	if refill == 0 then
	gui.pixelText(x+0,y+7, string.format("%4d", frefill), 0xFFFFFFFF, color.trans[0]) 
	else
	gui.pixelText(x+0,y+7, string.format("%4d", frefill), 0xFFFFFFFF, color.trans[2])
	end

-- In-game time (current segment)
	local igtframeseg = memory.read_u32_le(0x36DD, "IWRAM")
	gui.pixelText(x+180,y+0, string.format("%8d", igtframeseg), 0xFFFFFFFF, color.trans[0])
-- In-game time (current area)
	local igtframearea= memory.read_u32_le(0x3629, "IWRAM")
	gui.pixelText(x+180,y+7, string.format("%8d", igtframearea), 0xFFFFFFFF, color.trans[3])    
-- In-game time (global)
	local igtframe    = memory.read_u32_le(0x1DD8, "IWRAM")
	gui.pixelText(x+180,y+14, string.format("%8d", igtframe), 0xFFFFFFFF, color.trans[5])
	-- Global timer
	local globalframe = memory.read_u32_le(0x1DD4, "IWRAM")
	gui.pixelText(x+180,y+21, string.format("%8d", globalframe))

	-- Speed
	local xspeed      = memory.read_s32_le(0x3650, "IWRAM")
	local yspeed      = memory.read_s32_le(0x3654, "IWRAM")
	gui.pixelText(x+215,y, string.format("%6d", xspeed))
	gui.pixelText(x+215,y+7, string.format("%6d", yspeed))


	-- Ground/air? Displays G on ground, A on air
	local groair      = memory.read_u8(    0x3614, "IWRAM")
	if groair == 1 then
		gui.pixelText(x+215,y+14, "G", 0xFFFFFFFF, color.trans[4])
	else
		gui.pixelText(x+215,y+14, "A", 0xFFFFFFFF, color.trans[1])
	end

	-- Buffered down input timer to backdash
	local downbuffer  = memory.read_u8(    0x25C3, "IWRAM")
	if downbuffer >= 1 then
		gui.pixelText(x+219,y+14, "V" .. string.format("%4d", downbuffer), 0xFFFFFFFF, color.trans[0])
	else
		gui.pixelText(x+219,y+14, string.format("%5d", downbuffer), 0xFFFFFFFF, color.trans[5])
	end

	-- X and Y position
	local xposition   = memory.read_u32_le(0x3648, "IWRAM")
	local yposition   = memory.read_u32_le(0x364C, "IWRAM")
	gui.pixelText(x+203,y+35, string.format("%9d", xposition), 0xFFFFFFFF, color.trans[5])
	gui.pixelText(x+203,y+42, string.format("%9d", yposition), 0xFFFFFFFF, color.trans[5])

end

-- A visual separator for the sidebar
--#############################################################################
local function Separator(x,y, length)
--#############################################################################

	gui.drawLine(x+14,y, x+length,y)

	for i=0,6 do
		gui.drawPixel(x+i*2,y)
	end

end

-- General movie information
--#############################################################################
local function MovieInfo(x,y)
--#############################################################################

	local frame= emu.framecount()
	if emu.islagged() == true then
		gui.pixelText(x,y, frame, 0xFFFFFFFF, color.opaque[2])
	else
		gui.pixelText(x,y, frame)
	end

	local lagcount= emu.lagcount()
	gui.pixelText(x,y+7, lagcount, color.opaque[2])

	-- Shoutouts to Masterjun
	local buttons = {["Up"]="^", ["Down"]="v", ["Left"]="<", ["Right"]=">", ["Select"]="s", ["Start"]="S", ["A"]="A", ["B"]="B", ["L"]="L", ["R"]="R"}
	local s = ""
	for k,v in pairs(movie.getinput(frame-1)) do
		if v == true then
			s= s..buttons[k]
		end
	end
	
	gui.pixelText(x,y+14 ,s)

end

-- Displays information about Yoh's animations on the sidebar
--#############################################################################
local function GetYohState(x,y)
--#############################################################################

	local stateText= {
		[0]=  { "Standing","",""                   } ,
		      { "Walking","",""                    } ,
		      { "Ground","sliding",""              } ,
		      { "Crouching","",""                  } ,
		      { "Crouched","",""                   } ,
		      { "Standing","up",""                 } ,
		      { "Pre-","jumping",""                } ,
		      { "Jumping","",""                    } ,
		      { "Falling,","initial",""            } ,
		      { "Falling","",""                    } ,
		      { "Landing","",""                    } ,
		      { "Back","dashing",""                } ,
		      { "Entering","door",""               } ,
		      { "Exiting","door",""                } ,
		      { "Taking","damage",""               } ,
		      { "Taking","damage,","crouched"      } ,
		      { "Taking","damage,","air"           } ,
		      { "Knockback","damage",""            } ,
		      { "Knockback","upwards",""           } ,
		      { "Knockback","hitting","ground"     } ,
		      { "Knockback","fall",""              } ,
		      { "Knockback","ground",""            } ,
		      { "Knockback","getting up",""        } ,
		      { "Electro-","cuted",""              } ,
		      { "Electro-","cuted","2"             } ,
		      { "Taking","damage?",""              } ,
		      { "Taking","damage?",""              } ,
		      { "1st slash,","wooden,","ground"    } ,
		      { "2nd slash,","wooden,","ground"    } ,
		      { "3rd slash,","wooden,","ground"    } ,
		      { "1st slash,","light,","ground"     } ,
		      { "2nd slash,","light,","ground"     } ,
		      { "3rd slash,","light,","ground"     } ,
		      { "1st slash,","antiquity,","ground" } ,
		      { "2nd slash,","antiquity,","ground" } ,
		      { "3rd slash,","antiquity,","ground" } ,
		      { "Crouch","slash,","wooden"         } ,
		      { "Crouch","slash,","light"          } ,
		      { "Crouch","slash,","antiquity"      } ,
		      { "1st slash,","wooden,","air"       } ,
		      { "2nd slash,","wooden,","air"       } ,
		      { "3rd slash,","wooden,","air"       } ,
		      { "1st slash,","light,","air"        } ,
		      { "2nd slash,","light,","air"        } ,
		      { "3rd slash,","light,","air"        } ,
		      { "1st slash,","antiquity,","air"    } ,
		      { "2nd slash,","antiquity,","air"    } ,
		      { "3rd slash,","antiquity,","air"    } ,
		      { "Halo","Bump,","ground"            } ,
		      { "Halo","Bump,","air"               } ,
		      { "Nipopo","Punch", "ground"         } ,
		      { "Nipopo","Punch", "air"            } ,
		      { "Daodondo","",""                   } ,
		      { "Gussy", "Kenji", "ground"         } ,
		      { "Gussy", "Kenji", "air"            } ,
		      { "Jaguar","Swipe",""                } ,
		      { "Footballer","",""                 } ,
		      { "Footballer,","sparks",""          } ,
		      { "Big", "Thumb",""                  } ,
		      { "Big", "Thumb,","smoke"            } ,
		      { "Big", "Thumb,","Tokageroh"        } ,
		      { "Celestial","Slash,","ground"      } ,
		      { "Celestial","Slash,","air"         } ,
		      { "Celestial","Slash,","blade"       } ,
		[73]= { "Totem","Attack",""                } ,
		[82]= { "Shikigami,","ground",""           } ,
		[83]= { "Shikigami,","air",""              } ,
	}

        local state      = memory.read_u16_le(0x35E0, "IWRAM")
        local duration   = memory.read_u8(    0x35E9, "IWRAM")
        local statetimer = memory.read_u8(    0x35DF, "IWRAM")
        local delay      = memory.read_u16_le(0x362D, "IWRAM")

        local t          = stateText[state]
        if t then
                gui.pixelText(x,y, state .. ":" .. statetimer)
                gui.pixelText(x,y+ 7,t[1],color.opaque[1])
                gui.pixelText(x,y+14,t[2],color.opaque[1])
                gui.pixelText(x,y+21,t[3],color.opaque[1])
                gui.pixelText(x,y+28, duration .. ":" .. delay, 0xFFFFFFFF, color.trans[2])
	else
                gui.pixelText(x,y, state .. ":" .. statetimer)
                gui.pixelText(x,y+ 7,"NULL!!!",0xFFC0C0C0)
                gui.pixelText(x,y+28, duration .. ":" .. delay, 0xFFFFFFFF, color.trans[2])
	end

end

-- Information about our inventory (mediums, souls)
--#############################################################################
local function InventoryInfo(x,y)
--#############################################################################

	local leafcount = memory.read_u8(0x2358, "IWRAM")
	local rockcount = memory.read_u8(0x2359, "IWRAM")
	local dollcount = memory.read_u8(0x235A, "IWRAM")

	gui.pixelText(x,y, leafcount, color.opaque[0])
	gui.pixelText(x+11,y, rockcount, color.opaque[3])
        gui.pixelText(x+22,y, dollcount, color.opaque[5])

end

-- Draws hitboxes
-- Object size: B4 (180)
--#############################################################################
local function DrawHitbox(x,y, offset, id)
--#############################################################################

	local cameraX = memory.read_s32_le(0x1E50, "IWRAM")/256
	local cameraY = memory.read_s32_le(0x1E54, "IWRAM")/256

-- Figure out appropriate pixel values (somewhat hacky)
	local X1, X2= memory.read_s32_le(0x3664 + offset, "IWRAM")/256, memory.read_s32_le(0x3668 + offset, "IWRAM")/256
	local Y1, Y2= memory.read_s32_le(0x366C + offset, "IWRAM")/256, memory.read_s32_le(0x3670 + offset, "IWRAM")/256

	local pixelX1, pixelX2 = X1-cameraX+120, X2-cameraX+120
	local pixelY1, pixelY2 = cameraY-Y1+71, cameraY-Y2+71

	local invicibility = memory.read_u16_le(0x3630 + offset, "IWRAM")
	-- Add invicibility counter if invicible
	if invicibility >= 1 then
                gui.drawBox (x+pixelX1, y+pixelY1, x+pixelX2, y+pixelY2, color.opaque[1], color.trans[5])
                gui.pixelText (x+pixelX1, y+pixelY1, invicibility, 0xFFFFFFFF, color.trans[1])
	else
                gui.drawBox (x+pixelX1, y+pixelY1, x+pixelX2, y+pixelY2, color.opaque[5], color.trans[5])
	end

	-- Facing direction
	local fdirection = memory.read_u8(0x3634 + offset, "IWRAM")
	if fdirection    == 1 then
                gui.pixelText (x+pixelX1, y+pixelY2 - 7, "<")
	elseif fdirection == 0 then
                gui.pixelText (x+pixelX1, y+pixelY2 - 7, ">")
	else
                gui.pixelText (x+pixelX1, y+pixelY2 - 7, "?")
	end

	-- Raw damage output
	local rawdmg = memory.read_u8(0x3638 + offset, "IWRAM")
        gui.pixelText (x+pixelX1 + 8, y+pixelY2 - 7, rawdmg, 0xFFFFFFFF, color.trans[2])

	-- Health
	local health= memory.read_u16_le(0x3636 + offset, "IWRAM")
        gui.pixelText(x+pixelX1 + 10,y+pixelY1 + 1, health, color.opaque[1])

	-- State, animation and timer information
	if id ~= 0 then
                local state      = memory.read_u16_le(0x35E0 + offset, "IWRAM")
                local statetimer = memory.read_u8(0x35DF + offset, "IWRAM")
                local duration   = memory.read_u8(0x35E9 + offset, "IWRAM")
                local delay      = memory.read_u16_le(0x362D + offset, "IWRAM")
                gui.pixelText (x+pixelX1 + 24, y+pixelY2, state .. ":" .. statetimer)
                gui.pixelText (x+pixelX1 + 24, y+pixelY2 - 7, duration .. ":" .. delay, 0xFFFFFFFF, color.trans[2])
	end

end

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
while true do
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	DisplayHud(0,0)
	MovieInfo(241,0)
	Separator(241,22, 38)
	GetYohState(241,24)
	Separator(241,61, 38)
	InventoryInfo(241,63)

	for i=0,1 do
	--	if memory.read_u16_le(0x3636 + i*180, "IWRAM") ~= 0 then
			DrawHitbox(0,8, 0 + i*180, i)
	--	end
	end

	emu.frameadvance()
end
